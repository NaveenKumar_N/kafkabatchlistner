package com.gap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.gap")
public class KafkaListnerApplication {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListnerApplication.class);

  public static void main(String[] args) {
    LOGGER.info("-- <class> EventListnerApplication <method> main - Inited --");
    SpringApplication.run(KafkaListnerApplication.class, args);
  }
}
