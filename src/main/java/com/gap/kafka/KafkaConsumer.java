package com.gap.kafka;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

  @KafkaListener(topics = {"${spring.kafka.topicName.product}"})
  public void onMessage(ConsumerRecord<?, String> message, Consumer<?, ?> consumer) {
    LOGGER.info("-- <class> KafkaConsumer <method> onMessage() - message Recieved");
    System.out.println(
      "***********************************************************************************************************");
  }
}
