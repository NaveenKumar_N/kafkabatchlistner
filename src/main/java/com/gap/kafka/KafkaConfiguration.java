package com.gap.kafka;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.util.backoff.ExponentialBackOff;

@Configuration
@EnableKafka
public class KafkaConfiguration {

  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConfiguration.class);

  @Value("${spring.kafka.consumer.bootstrap-servers}")
  private String bootstrapServers;

  @Value("${spring.kafka.consumer.key-deserializer}")
  String keyDeserializerClass;

  @Value("${spring.kafka.consumer.value-deserializer}")
  String valueDeserializerClass;

  @Bean
  public Map<String, Object> consumerConfigs() {

    LOGGER.info("-- <class> KafkaConfiguration <method> consumerConfigs() - initiated --");

    Map<String, Object> props = new HashMap<>();
    try {
      // list of host:port pairs used for establishing the initial connections to the Kafka cluster
      props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
        bootstrapServers);
      props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
        Class.forName(keyDeserializerClass));
      props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
        Class.forName(valueDeserializerClass));
      // allows a pool of processes to divide the work of consuming and processing records
      props.put(ConsumerConfig.GROUP_ID_CONFIG, "kafkaListner");
      // automatically reset the offset to the earliest offset
      props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    } catch (Exception e) {
      LOGGER.info(" : <> Error {}", e);
    }

    return props;
  }

  @Bean
  public ConsumerFactory<String, String> consumerFactory() {
    LOGGER.info("-- <class> KafkaConfiguration <method> consumerFactory() - initiated --");

    return new DefaultKafkaConsumerFactory<>(consumerConfigs());
  }

  @Bean
  public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
    LOGGER.info("-- <class> KafkaConfiguration <method> kafkaListenerContainerFactory() - initiated --");
    ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
    SeekToCurrentErrorHandler seekToCurrentErrorHandler = new SeekToCurrentErrorHandler(new ExponentialBackOff(0L, 3L));
    factory.setConsumerFactory(consumerFactory());
    factory.setRecoveryCallback(context -> {
      new DeadLetterPublishingRecoverer(getKafkaTemplate()).accept(
        (ConsumerRecord<?, ?>) context.getAttribute("record"),
        (Exception) context.getLastThrowable());
      return null;
    });
    factory.setErrorHandler(seekToCurrentErrorHandler);
    return factory;
  }

  @Bean
  public KafkaConsumer receiver() {
    LOGGER.info("-- <class> KafkaConfiguration <method> receiver() - initiated --");
    return new KafkaConsumer();
  }

  @Bean
  public ProducerFactory<String, String> producerFactory() {
    return new DefaultKafkaProducerFactory<>(consumerConfigs());
  }

  @Bean
  public KafkaTemplate<String, String> getKafkaTemplate() {
    return new KafkaTemplate<>(producerFactory());
  }
}
