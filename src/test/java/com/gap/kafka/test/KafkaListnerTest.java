package com.gap.kafka.test;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gap.KafkaListnerApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = KafkaListnerApplication.class,
  properties = {"spring.cloud.zookeeper.enabled=false", "spring.cloud.zookeeper.config.watcher.enabled=false"})
@TestPropertySource(locations = {"classpath:application-dev.yml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("dev")
@EmbeddedKafka(
  partitions = 1,
  controlledShutdown = false,
  brokerProperties = {
    "listeners=PLAINTEXT://localhost:3333",
    "port=3333"
  })
public class KafkaListnerTest {

  private static final String TOPIC = "price";

  public final static ObjectMapper mapper = new ObjectMapper();

  @ClassRule
  public static EmbeddedKafkaRule embeddedKafka = new EmbeddedKafkaRule(1, true, TOPIC);

  private KafkaTemplate<String, String> template;

  @Autowired
  EmbeddedKafkaBroker embeddedKafkaBroker;

  @Autowired
  KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

//  /** The data path. */
//  String productDataPath = "src/test/resources/data/ProductMessageData.json";

  /** The consumerMessage json. */
  public static JSONObject productMessageJson = null;

  @Before
  public void setup() throws Exception {

    ProducerFactory<String, String> producerFactory = new DefaultKafkaProducerFactory<>(
      KafkaTestUtils.producerProps(embeddedKafkaBroker));

    template = new KafkaTemplate<>(producerFactory);
    template.setDefaultTopic(TOPIC);
  }

  @Test
  public void test01KafkaConsumerTest() throws Exception {

    template.sendDefault("string");
  }

}
